# Nguyen_Julie_M1Dev_2020_docker

<img src="https://cdn.iconscout.com/icon/free/png-512/docker-226091.png"  width="120" height="120">

<img src="https://codingthesmartway.com/wp-content/uploads/2019/01/mern_logo.png" width="20%">

<img src="https://static.wixstatic.com/media/689356_6002e82c98d54c64948c2e323896032c~mv2.png/v1/fill/w_208,h_208,al_c,q_85,usm_0.66_1.00_0.01/MyDigitalSchool.webp"  width="120" height="120">

## Introduction

Dans le cadre de ma formation en Master en tant que Developpeur full stack, je suis un module sur l'utilisation de Docker.
Ce repository contient le rendu de mon projet sur Docker.

Rendu le 05/10/2020.

[Sujet du projet](https://slack-files.com/T019ZCC803H-F01BXFK39HN-123892e20e)

Le but de ce projet est d’intégrer le déploiement d’une API Rest avec Docker afin de pouvoir l’intégrer
dans un système d’intégration continue et de développement continue.

## Technologies utilisées

- Back-end : NodeJs - Express
- Front-end : ReactJs
- Database NoSQL : MongoDB
- Deploiement : Docker

## Docker Images

Possibilité de récuperer ces images via la commande : 

Liens sur l'image Frontend --> [DockerHub Image Frontend](https://hub.docker.com/repository/docker/goulax/nguyen_julie_m1dev_2020_docker_frontend)

```bash
docker pull goulax/nguyen_julie_m1dev_2020_docker_frontend
```

Liens sur l'image Backend --> [DockerHub Image Backend](https://hub.docker.com/repository/docker/goulax/nguyen_julie_m1dev_2020_docker_backend)

```bash
docker pull goulax/nguyen_julie_m1dev_2020_docker_backend
```

## Utilisation de l'API REST

Qu'est-ce qu'un dockerfile ? 
-> Dockerfile est de définir de quelle image vous héritez.

- Nguyen_Julie_M1Dev_2020_docker
    - frontend
        - Dockerfile
        ```bash
        // FROM permet de définir notre image de base, vous pouvez l'utiliser seulement une fois dans un Dockerfile.
        FROM node:alpine

        LABEL maintainer="NGUYEN Julie"

        // WORKDIR permet de changer le répertoire courant de votre image, toutes les commandes qui suivront seront exécutées à partir de ce répertoire.
        WORKDIR /app

        // COPY permet de copier le fichier souhaité dans le container.
        COPY package*.json ./

        // RUN permet d'exécuter une commande à l'intérieur de votre image comme si vous étiez devant un shell.
        RUN npm install --save

        COPY . .

        // EXPOSE permet d'indiquer sur quel port nous souhaitons partager.
        EXPOSE 3000

        // ENTRYPOINT permet d'indiquer quelle instruction doit s'exécuter au lancement de votre conteneur, il peut être accompagné de la commande CMD pour ajouté davantage de paramètre d'exécution.
        ENTRYPOINT [ "npm", "start" ]
        ```

    - backend
        - Dockerfile
        ```bash
        FROM node
        LABEL maintainer="NGUYEN Julie"
        WORKDIR /app
        COPY package*.json ./
        RUN npm install --save
        COPY . .
        EXPOSE 8080
        ENTRYPOINT [ "npm", "start" ]
        ```

    - docker-compose.yml
    ```bash
    // "version" indique la version de docker utilisé.
    version: '2'
    // "service" est une section décrivant les services à exécuter
    services: 
        // Nom du container
        frontend:
            // Ici, on peut customiser le nom du container
            container_name: "frontend"

            // "build" qui permet de spécifier le Dockerfile source pour créer l'image du conteneur.
            build: ./frontend

            // On renseigne ici, sur quel port il va être lancé
            ports:
                - '3000:3000'

            // On relie ici le container "frontend" au container "backend"
            links:
                - backend

        // Le container du backend
        backend:
            container_name: "backend"
            build: ./backend
            ports:
                - '8080:8080'
            links:
                - mongo

        // Le container mongo
        mongo:
            container_name: mongodb
            image: mongo
            ports:
                - '27017:27017'

    ```
    - README.md

### Installation

#### Installation de Docker

1. Installer Docker-cli ou Docker Desktop sur votre machine.

2. Connectez vous avec vos identifiant Docker sur Docker-CLI ou sur Docker Desktop.

3. Vous pouvez maintenant utiliser les commandes Docker !

#### Installation de l'API Rest avec Docker

1. Cloner le repository sur votre machine

```bash
git clone https://gitlab.com/Kyulee/nguyen_julie_m1dev_2020_docker.git
```

2. Cette commande va permettre d'initialiser et démarrer les 3 containers :
- 1er Container : Frontend
- 2ème Container : Backend
- 3ème Container : MongoDB 

```bash
docker-compose up --build
```

3. Rendez vous dans votre browser/navigateur internet et taper l'url suivant : 
http://localhost:3000

4. Vous pouvez désormais vous enregister, vous connecter et poster ce que vous voulez !

5. Vous pouvez également exécuter la commande ci-dessous pour voir que vos container sont actuellement en train de tourner.
```bash
docker ps -a
```
6. Les autres commandes docker : 

Pour runner un container :
```bash
docker start <ID du container> ou <le nom du container>
```
Pour arreter un container : 
```bash
docker stop <ID du container> ou <le nom du container>
```
Pour supprimer un container : 
```bash
docker rmi <ID du container> ou <le nom du container>
```

Pour information, si vous utilisez Docker Desktop, toutes ces commandes peuvent être exécuter directement sur l'interface.

